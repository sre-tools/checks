#!/bin/bash

error=0
base_repo=https://gitlab.com/sre-tools/checks/raw/master

echo '=============== rubocop ================'
if ! rubocop; then
  ((error++))
fi

echo '============== foodcritic =============='
if ! foodcritic -f any .; then
  ((error++))
fi

echo '============= lines length ============='
if ! curl -s $base_repo/check_lines_length.sh | \
  bash -s -- 80 '\./\.kitchen'; then
  ((error++))
fi

echo '============= git_history =============='
if ! curl -s $base_repo/check_git_history.sh | \
  bash; then
  ((error++))
fi

echo '=============== release ================'
if [ ! -s .category ]; then
  echo 'chore: add supermarket category in .category'
  echo 'See https://docs.chef.io/knife_cookbook_site.html#share for the list'
  ((error++))
fi

echo '================= misc ================='
if [ -e '.kitchen.yml' ]; then
  echo 'test: rename kitchen config file without dot'
  ((error++))
fi

if ! grep skip_preparation kitchen.yml > /dev/null; then
  if ! grep 'name: vagrant' kitchen.yml > /dev/null; then
    echo '==> Add skip_preparation!'
    ((error++))
  fi
fi

if ! grep 'include:' .gitlab-ci.yml > /dev/null; then
  if ! grep "name 'test-cookbook'" metadata.rb > /dev/null; then
    echo 'test: include .gitlab-ci.yml from test-cookbook';
    ((error++))
  fi
fi

if [ -e 'test/kitchen_command.rb' ]; then
  if ! grep 'rescue StandardError' 'test/kitchen_command.rb' > /dev/null; then
    echo '==> test: use test/kitchen_command.rb from test-cookbook'
    ((error++))
  fi
fi

if ! grep 'always_update_cookbooks' kitchen.yml > /dev/null; then
  if ! grep 'name: vagrant' kitchen.yml > /dev/null; then
    echo '==> Set always_update_cookbooks: true'
    ((error++))
  fi
fi

if ! grep 'build_pull: true' kitchen.yml > /dev/null; then
  if ! grep 'name: vagrant' kitchen.yml > /dev/null; then
    echo '==> Set build_pull: true'
    ((error++))
  fi
fi

if grep 'require_chef_omnibus: false' kitchen.yml > /dev/null; then
  echo '==> test: replace deprecated require_chef_omnibus'
  ((error++))
fi

if grep 'image: sbernard' kitchen.yml > /dev/null; then
  echo 'test: switch to chefplatform images'
  ((error++))
fi

if grep 'image: makeorg' kitchen.yml > /dev/null; then
  echo 'test: switch to chefplatform images'
  ((error++))
fi

if git branch -a | grep develop > /dev/null; then
  echo '==> Please remove branch develop'
  ((error++))
fi

attributes=attributes
if ! ls attributes > /dev/null 2>&1; then
  attributes=''
fi

if ! grep -r cookbook_name $attributes > /dev/null; then
  echo '==> sed cookbook_name everywhere'
  ((error++))
fi

if ! find . -type f -print0 | \
  xargs -0 grep 'Copyright (c) .*2021' > /dev/null; then
  echo 'chore: add 2021 to copyright notice'
  ((error++))
fi

if grep -r 'samuel.bernard@s4m.io' . > /dev/null; then
  echo '==> samuel.bernard@s4m.io is not valid anymore ;)'
  ((error++))
fi

if grep -r 'florian.philippon@s4m.io' . > /dev/null; then
  echo '==> Use florian.philippon@gmail.com instead of s4m.io one'
  ((error++))
fi

if grep -r 'dps@s4m.io' . > /dev/null; then
  echo '==> Change maintainer email in metadata!'
  ((error++))
fi

if grep 'gitlab.com/s4m-chef-repositories' metadata.rb > /dev/null; then
  echo '==> Change source & issues url to chef-platform'
  ((error++))
fi

if grep -r 's4m' . | grep -v '\./\.git'; then
  echo '==> Remove all mention of s4m, chore: handover maintenance to make.org'
  ((error++))
fi

package="node\[cookbook_name\]\['package_retries'\]"
if grep -r "retries $package$" recipes > /dev/null; then
  echo '==> Use unless .nil? to make package_retries works by default'
  ((error++))
fi

if ! grep 'karma' CONTRIBUTING.md > /dev/null; then
  echo '==> chore: set new contributing guide with karma style'
  ((error++))
fi

if grep 'docs' CONTRIBUTING.md > /dev/null; then
  echo '==> doc: use doc in git message instead of docs'
  ((error++))
fi

if ! grep 'rubocop' Gemfile > /dev/null; then
  echo '==> chore: add foodcritic & rubocop in Gemfile'
  ((error++))
fi

if grep 'molinillo' Gemfile > /dev/null; then
  echo '==> Use latest Gemfile without molinillo constraint'
  ((error++))
fi

if ! grep "maintainer 'Chef Platform'" metadata.rb > /dev/null; then
  echo 'chore: set generic maintainer & helpdesk email'
  ((error++))
fi

echo '================= end =================='
echo "DO NOT FORGET the git clean -fxd"

exit $error
