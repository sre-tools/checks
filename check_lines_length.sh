#!/bin/bash

limit=$1
if [ "$limit" = "" ]; then
  limit=80
fi

shift
exceptions="$@"
exceptions="${exceptions// /|}"
if [ "$exceptions" = "" ]; then exceptions='^$'; fi

wcfiles() {
  find . -type f | grep -v '\.git' | grep -v -E "$exceptions" | xargs wc -L
}

filter() {
  while read line; do
   length=$(echo $line | cut -f1 -d' ')
   if [ $length -ge $limit ]; then
     echo $line
   fi
  done | head -n -1
}

max=$(wcfiles | tail -n 1 | xargs echo | cut -f1 -d' ')

if [ $max -ge $limit ]; then
  echo "ERROR: at least one file has a line longer or equal to $limit chars:"
  wcfiles | filter
  exit 1
fi

echo "OK: lines length < $limit chars "
exit 0
