#!/bin/bash

RED='\e[0;31m'   # Red
GREEN='\e[0;32m' # Green
NORMAL='\e[0m'   # Text Reset

usage() {
  echo -e "Check if git history is correct.\n"
  echo "$0 scope"
  echo -e "  where scope is a git log scope like master..HEAD or nothing\n"

  echo -e "List of checks:

-  No trailing whitespace and endline (git show --check)
-  Title width between 10 and 75 chars
-  Second line of description is empty
-  Description width is less than 72 chars

Read Contributing Guide for more information"
}

wc-L() {
  awk '{print length, $0}' | sort -nr | head -1 | cut -d' ' -f1
}


checkgit() {
  local scope=$@

  local scopename="$scope"
  if [ "$scopename" = "" ]; then
    scopename="everything"
  fi
  echo -e "Testing history on $scopename…\n"

  ERROR=false
  local commit_fail=false
  FORMAT_ERROR=""
  local commit_format=""

  while read commit; do
    commit_fail=false
    commit_format=""
    local short=$(git show -s --pretty=format:"%h" $commit)

    if ! git show --check $commit > /dev/null; then
      commit_format="$commit_format
    + Whitespace checks failed! Use \`git show --check $short\`"
      commit_fail=true
    fi

    local title="$(git show -s --pretty=format:'%B' $commit | head -n 1)"
    if [ $(echo "$title" | wc-L) -gt 75 ]; then
      commit_format="$commit_format
    + Commit title is $(echo $title | wc-L) chars wide (limit is 75 chars):
          $title"
      commit_fail=true
    fi

    if [ $(echo "$title" | wc-L) -lt 10 ]; then
      commit_format="$commit_format
    + Commit title is really short. You should do better (at least 10 chars):
          $title"
      commit_fail=true
    fi

    if [ $(git show -s --pretty=format:'%B' $commit | wc -l) -gt 1 ]; then
      local second="$(git show -s --pretty=format:'%B' $commit | head -n 2 | \
        tail -n 1)"
      if [ $(echo $second | wc-L) -ne 0 ]; then
        commit_format="$commit_format
    + Second line of a commit description must be empty, here it is:
          $second"
        commit_fail=true
      fi
    fi

    local line_len=$(git show -s --pretty=format:"%b%n" $commit | wc-L)
    if [ $line_len -gt 72 ]; then
      line="$(git show -s --pretty=format:'%b' $commit | \
        awk '{if(length>x){x=length;l=$0}}END{print l}')"
      commit_format="$commit_format
    + Lines width must be <= 72 chars. The following is wider ($line_len):
          $line"
      commit_fail=true
    fi

    if [ "$commit_fail" = "true" ]; then
      ERROR=true
      FORMAT_ERROR="$FORMAT_ERROR
- commit \`$short\`:$commit_format\n"
    fi
  done < <(git log --pretty=tformat:"%H" $scope)

  if [ "$ERROR" = "true" ]; then
    FORMAT_ERROR="${RED}There are some problems with commits log:${NORMAL}
$FORMAT_ERROR
I invite you to use \`git rebase -i\` to fix theses issues.
See $DOC_URL for more information."
    echo -e "${FORMAT_ERROR}"
    return 1;
  else
    echo -e "${GREEN}Everything is fine! :)${NORMAL}"
  fi
}

if [[ $1 == -* ]]; then
  usage
else
  checkgit $@
fi
