#!/usr/bin/fish

set date 2018

find . -type f -not -iwholename '*.git*' -print0 | \
xargs -0 sed -i \
  -e "s/\(Copyright (c).* 201[0-9]\) Make.org\$/\1-$date Make.org/g"\
  -e "s/\(Copyright (c) .* 201[0-9]\)-201[0-9] Make.org\$/\1-$date Make.org/g"\
  -e "s/\(Copyright (c) .*Sam4Mobile\)\$/\1, $date Make.org/g"
